/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author alumno
 */
public class Conexion {

    public static Connection mySQL(String baseDatos, String username, String password) {
        Connection con = null;
        try {
            // 1.1) Cargamos el driver JDBC que vayamos a usar
            Class.forName("com.mysql.jdbc.Driver");
            // 1.2) Establecemos una conexi�n con nuestra base de datos 
            //El objeto Properties hace que salgan tildes y e�es, tambi�n podemos a�adir el usuario y contrase�a 
            Properties props = new Properties();
            props.put("charSet", "iso-8859-1");
            props.put("user", username);
            props.put("password", password);
            //conectamos con la base de datos 
            String url = "jdbc:mysql://localhost:3306/" + baseDatos;
            con = (Connection) java.sql.DriverManager.getConnection(url, props);
            return con;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No hay conexi�n con la base de datos");
        }
        return null;
    }

    public static Connection derby(String baseDatos, String username, String password) {
        Connection con = null;
        try {
            // 1.1) Cargamos el driver Conexion que vayamos a usar 
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            //especificamos el driver 
            // 1.2) Establecemos una conexi�n con nuestra base de datos 
            //El objeto Properties hace que salgan tildes y e�es, tambi�n podemos a�adir el usuario y contrase�a 
            Properties props = new java.util.Properties();
            props.put("charSet", "iso-8859-1");
            props.put("user", username);
            props.put("password", password);
            //conectamos con la base de datos 
            String url = "jdbc:derby://localhost:1527/" + baseDatos;
            con = (Connection) DriverManager.getConnection(url, props);
            return con;
        } catch (java.sql.SQLException e) {
            System.out.println("SQL Exception: " + e.toString());
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "No hay conexi�n con la base de datos");
        }
        return null;
    }

    public static Connection derbyEmbebida(String baseDatos, String username, String password) {
        Connection con = null;
        try {
            // 1.1) Cargamos el driver Conexion que vayamos a usar 
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            // 1.2) Establecemos una conexi�n con nuestra base de datos 
            //El objeto Properties hace que salgan tildes y e�es, tambi�n podemos a�adir el usuario y contrase�a 
            java.util.Properties props = new java.util.Properties();
            props.put("charSet", "iso-8859-1");
            props.put("user", username);
            props.put("password", password);
            //conectamos con la base de datos 
            String url = "jdbc:derby:" + baseDatos;
            con = (Connection) java.sql.DriverManager.getConnection(url, props);
            return con;
        } catch (java.sql.SQLException e) {
            System.out.println("SQL Exception: " + e.toString());
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "No hay conexi�n con la base de datos");
        }
        return null;
    }

    //Este tipo de conexi�n es s�lo soportada por la extensi�n de Mozilla Firefox SQlite, usal� s�lo en caso de que tengas SQlite.
    /*  
    public static Connection sqlite(String baseDatos) {
        Connection con = null;

        try {
            // 1.1) Cargamos el driver Conexion que vayamos a usar 
            Class.forName("org.sqlite.JDBC");
            //especificamos el driver 
            // 1.2) Establecemos una conexi�n con nuestra base de datos 
            //El objeto Properties hace que salgan tildes y e�es, tambi�n podemos a�adir el usuario y contrase�a
            java.util.Properties props = new java.util.Properties();
            props.put("charSet", "iso-8859-1");
            //conectamos con la base de datos 
            String url = "jdbc:sqlite:" + baseDatos;
            con = (java.sql.Connection) java.sql.DriverManager.getConnection(url, props);
            return con;
        } catch (java.sql.SQLException e) {
            System.out.println("SQL Exception: " + e.toString());
            return null;
        } catch (ClassNotFoundException e) {
            System.out.println("Class Not Found Exception: " + e.toString());
            return null;
        }
    }
    */

}
